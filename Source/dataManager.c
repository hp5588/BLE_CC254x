#include "hal_flash.h" 


#define FLASH_PAGE_UUID 100
#define FLASH_ADDRESS_UUID 0x4600
//#define FLASH_ADDRESS_UUID (FLASH_PAGE_UUID-1)*256 //2048/8 =256
//#deifine TEST_DATA 0x11223344

static uint8 *selfDefUUID;


void saveData(uint8* data,uint8 cn){
  HalFlashWrite(FLASH_ADDRESS_UUID,data,cn);
}

void dataManagerInit(){
  //Alloc Space for Variants
  selfDefUUID = (uint8*)osal_mem_alloc(6);
  
  //DEBUG Use
  static uint8 Data[4] = {0x11,0x22,0x33,0x47};
  saveData(Data,4);

  //Initiate Values
  HalFlashRead(FLASH_PAGE_UUID,0,selfDefUUID,6);

}


