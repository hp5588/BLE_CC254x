 
#ifndef DOORLOCKCONTROLLER_H
#define DOORLOCKCONTROLLER_H

extern void DoorLockController_Init( uint8 task_id );
extern uint16 DoorLockController_ProcessEvent( uint8 task_id, uint16 events );

#endif